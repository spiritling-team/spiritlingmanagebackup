const path = require("path");
const CircularDependencyPlugin = require("circular-dependency-plugin");
const lessLocalIdentName = "sl_[name]_[local]_[sha512:hash:base62:16]";

module.exports = {
    style: {
        modules: {
            localIdentName: lessLocalIdentName,
        },
    },
    webpack: {
        // alias: {
        //     "@global": path.resolve(__dirname, "src/assets"),
        // },
        plugins: [
            new CircularDependencyPlugin({
                onStart({ compilation }) {
                    console.log("start detecting webpack modules cycles");
                },
                // `onDetected` is called for each module that is cyclical
                onDetected({
                    module: webpackModuleRecord,
                    paths,
                    compilation,
                }) {
                    // `paths` will be an Array of the relative module paths that make up the cycle
                    // `module` will be the module record generated by webpack that caused the cycle
                    compilation.errors.push(new Error(paths.join(" -> ")));
                },
                // `onEnd` is called before the cycle detection ends
                onEnd({ compilation }) {
                    console.log("end detecting webpack modules cycles");
                },
                // exclude detection of files based on a RegExp
                exclude: /a\.js|node_modules/,
                // include specific files based on a RegExp
                include: /src/,
                // add errors to webpack instead of warnings
                failOnError: true,
                // allow import cycles that include an asyncronous import,
                // e.g. via import(/* webpackMode: "weak" */ './file.js')
                allowAsyncCycles: false,
                // set the current working directory for displaying module paths
                cwd: process.cwd(),
            }),
        ],
    },
    babel: {
        plugins: [
            [
                "import",
                {
                    libraryName: "@ant-design",
                    libraryDirectory: "icons",
                    camel2DashComponentName: false, // default: true
                },
            ],
        ],
    },
};
