import React, { Suspense, useEffect } from "react";
import { getHostPath, getRelativeBasePath } from "utils/envDetect";
import { Prompt, Redirect, Route, Switch } from "react-router-dom";
import { useSelector } from "react-redux";
import { authSelector, routerSelector, statusSelector } from "store/selector";
import GlobalLoading from "components/GlobalLoading/GlobalLoading";
import routerConfig from "routes/router";
import LayoutIndex from "layout/LayoutIndex";
import { localStorageUtils } from "utils/localStorageUtils";
import { LocalAuthAccessToken } from "consts/localStorageName";
import _ from "lodash";
import { IAuthToken, IdentityServerConfiguration } from "types/authInterface";
import {
    checkLoginStatus,
    getIdsConfigAsync,
    getUserInfoAsync,
    initToken,
} from "rtk/authRtk";
import { useAppDispatch } from "utils/appDispatchAndSelector";
import { getConnectAuthorize } from "utils/endpoint";
import { getAccessToken } from "utils/getStore";
import { IRouter } from "utils/routerUtils";
import { setAppInit, setLoadingStatus } from "rtk/statusRtk";
import { v4 as uuid } from "uuid";
import Logger from "utils/_logger";

function buildRouter(_routes: IRouter[]): JSX.Element[] {
    return _routes.map((_router) => {
        if (_router.routes && _router.routes.length > 0) {
            return (
                <React.Fragment>
                    <Route
                        path={_router.path}
                        key={_router.name}
                        component={_router.component}
                        {..._router.routeOption}
                    />
                    {buildRouter(_router.routes)}
                </React.Fragment>
            );
        } else {
            return (
                <Route
                    path={_router.path}
                    key={_router.name}
                    component={_router.component}
                    {..._router.routeOption}
                />
            );
        }
    });
}

// 渲染需要登录的路由
function SuccessContent() {
    console.log(buildRouter(routerConfig.manageRouter));
    return (
        <Route>
            <Suspense fallback={<GlobalLoading key={uuid()} />}>
                <LayoutIndex>
                    <Switch>
                        <Route path={"/"} exact={true}>
                            <Redirect to={routerConfig.manageRouter[0].path} />
                        </Route>
                        {buildRouter(routerConfig.manageRouter)}
                    </Switch>
                </LayoutIndex>
            </Suspense>
        </Route>
    );
}

function App() {
    const status = useSelector(statusSelector);
    const auth = useSelector(authSelector);
    const routerState = useSelector(routerSelector);
    InitLoad(routerState.location.pathname);
    return (
        <React.Fragment>
            <Prompt when={false} message={"message"} />
            {status.loading && <GlobalLoading message={status.loadingText} />}
            <Switch>
                {buildRouter(routerConfig.publicRouter)}
                {auth.isLogin && <SuccessContent key={uuid()} />}
            </Switch>
        </React.Fragment>
    );
}

function InitLoad(pathname: string) {
    const dispatch = useAppDispatch();

    // 进行获取认证等操作
    useEffect(
        function () {
            const init = async function () {
                const accessToken =
                    localStorageUtils.getItem(LocalAuthAccessToken);
                const routerList = routerConfig.publicRouter.map((x) => x.path);
                const _idsConfig = await dispatch(getIdsConfigAsync());
                const idsConfig =
                    _idsConfig.payload as IdentityServerConfiguration;
                const isNotSingle = _.indexOf(routerList, pathname) == -1;

                if (accessToken && isNotSingle) {
                    dispatch(initToken());
                    const _userIfno = await dispatch(
                        getUserInfoAsync({
                            userInfoEndpoint: idsConfig.userinfo_endpoint,
                            accessToken: getAccessToken(),
                        })
                    );
                    const userInfo = _userIfno.payload as
                        | IAuthToken
                        | undefined;
                    // 用户信息是否可以获取到
                    if (userInfo) {
                        // 已登录
                        await dispatch(checkLoginStatus(true));
                        await dispatch(setLoadingStatus(false));
                        await dispatch(setAppInit(true));
                    } else {
                        const url = getConnectAuthorize(
                            idsConfig.authorization_endpoint
                        );
                        // 跳转到connect/authorize
                        window.open(url, "_self");
                    }
                } else {
                    if (isNotSingle) {
                        const url = getConnectAuthorize(
                            idsConfig.authorization_endpoint
                        );
                        window.open(url, "_self");
                    } else {
                        await dispatch(setAppInit(true));
                    }
                }
            };
            try {
                init();
            } catch (err) {
                Logger.LogInformation("App", "Init", err);
            }
        },
        [dispatch]
    );
}

export default App;
