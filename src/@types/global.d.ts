/**
 * @description 全局扩展接口
 */
interface Window {
    /**
     * @description 阿里巴巴矢量库地址
     */
    _ICON_FONT_URL: string;
    _IDENTITY_SERVER_URL: string;
    _API_END_POINT: string;
    _devHelper: any;
}
