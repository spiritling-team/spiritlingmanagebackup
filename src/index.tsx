import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { store } from "store/store";
import { Provider } from "react-redux";
import { initDevHelper } from "utils/devHelper";
import { ConnectedRouter } from "connected-react-router";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import LocalizedFormat from "dayjs/plugin/localizedFormat";
import { history } from "store";
import { isDev } from "utils/envDetect";
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
import { ConfigProvider } from "antd";
import zhCN from "antd/lib/locale/zh_CN";
import "dayjs/locale/zh-cn";

dayjs.extend(relativeTime);
dayjs.extend(LocalizedFormat);
dayjs.locale("zh-cn");

Sentry.init({
    dsn: "https://021a47e6564141f7a908f240a695f407@o335651.ingest.sentry.io/5872190",
    integrations: [new Integrations.BrowserTracing()],
    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
    environment: "system:manage",
});

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <ConfigProvider locale={zhCN}>
                <App />
            </ConfigProvider>
        </ConnectedRouter>
    </Provider>,
    document.getElementById("root")
);

if (isDev) {
    initDevHelper(store);
}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
