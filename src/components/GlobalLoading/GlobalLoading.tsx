import React from "react";
import styles from "./globalLoading.module.scss";
import loadingImag from "assets/image/loading.gif";

export interface IGlobalLoadingProps {
    message?: string;
}

const GlobalLoading = (props: IGlobalLoadingProps) => {
    const message = props.message ?? "正在加载中...";
    return (
        <div className={styles.loadingWrapper}>
            <div className={styles.loadingContainer}>
                <img
                    src={loadingImag}
                    className={styles.image}
                    alt={"loading gif"}
                />
                <br />
                <span className={styles.text}>{message}</span>
            </div>
        </div>
    );
};

export default GlobalLoading;
