import {
    IAuthToken,
    IdentityServerConfiguration,
    IGetTokenParams,
    IUserInfo,
} from "types/authInterface";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { localStorageUtils } from "utils/localStorageUtils";
import {
    LocalAuthAccessToken,
    LocalAuthConfig,
    LocalAuthToken,
    LocalAuthUserInfo,
} from "consts/localStorageName";
import axios from "axios";
import * as queryString from "query-string";
import _ from "lodash";

export interface IAuthRtkState {
    token?: IAuthToken;
    isLogin: boolean;
    userInfo?: IUserInfo;
    idsConfig?: IdentityServerConfiguration;
}

const initialState: IAuthRtkState = {
    isLogin: false,
};

export const getTokenAsync = createAsyncThunk(
    "auth/getTokenAsync",
    async (tokenParams: IGetTokenParams): Promise<IAuthToken | undefined> => {
        const tokenEndpoint = tokenParams.endPoint;
        if (tokenEndpoint) {
            delete tokenParams.endPoint;
            try {
                const params = new URLSearchParams();
                _.transform(tokenParams, (r, v, k) => {
                    params.append(k, v ?? "");
                });
                const result = await axios.post<IAuthToken>(
                    tokenEndpoint,
                    params,
                    {
                        data: queryString.stringify(tokenParams),
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded",
                        },
                    }
                );
                return result.data;
            } catch (err) {
                return undefined;
            }
        } else {
            return undefined;
        }
    }
);

export const getUserInfoAsync = createAsyncThunk(
    "auth/getUserInfoAsync",
    async (params: {
        userInfoEndpoint?: string;
        accessToken?: string;
    }): Promise<IUserInfo | undefined> => {
        if (params.userInfoEndpoint) {
            try {
                const result = await axios.get<IUserInfo>(
                    params.userInfoEndpoint,
                    {
                        headers: {
                            Authorization: `Bearer ${params.accessToken}`,
                        },
                    }
                );
                return result.data;
            } catch (err) {
                return undefined;
            }
        } else {
            return undefined;
        }
    }
);

export const getIdsConfigAsync = createAsyncThunk(
    "auth/getIdsConfigAsync",
    async (): Promise<IdentityServerConfiguration | undefined> => {
        try {
            const result = await axios.get(
                window._IDENTITY_SERVER_URL + ".well-known/openid-configuration"
            );
            return result.data;
        } catch (err) {
            return undefined;
        }
    }
);

export const AuthSlice = createSlice({
    name: "Auth",
    initialState,
    reducers: {
        initToken: (state) => {
            if (state.token) {
                return state;
            } else {
                state.token =
                    localStorageUtils.getItemForAny<IAuthToken>(LocalAuthToken);
                return state;
            }
        },
        checkLoginStatus: (state, action: PayloadAction<boolean>) => {
            state.isLogin = action.payload;
            return state;
        },
    },
    extraReducers: (builder) => {
        builder.addCase(getTokenAsync.fulfilled, (state, action) => {
            if (!action.payload) {
                state.isLogin = false;
            } else {
                localStorageUtils.setItemByAny(LocalAuthToken, action.payload);
                localStorageUtils.setItemByAny(
                    LocalAuthAccessToken,
                    action.payload.access_token
                );
                state.token = action.payload;
                state.isLogin = true;
            }
            return state;
        });
        builder.addCase(getIdsConfigAsync.fulfilled, (state, action) => {
            state.idsConfig = action.payload;
            localStorageUtils.setItem(
                LocalAuthConfig,
                JSON.stringify(action.payload)
            );
            return state;
        });
        builder.addCase(getUserInfoAsync.fulfilled, (state, action) => {
            state.userInfo = action.payload;
            localStorageUtils.setItem(
                LocalAuthUserInfo,
                JSON.stringify(action.payload)
            );
            return state;
        });
    },
});

export const { initToken, checkLoginStatus } = AuthSlice.actions;

export default AuthSlice.reducer;
