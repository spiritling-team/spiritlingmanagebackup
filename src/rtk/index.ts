import { combineReducers } from "@reduxjs/toolkit";
import StatusReducer, { IStatusRtkState } from "./statusRtk";
import AuthReducer, { IAuthRtkState } from "rtk/authRtk";

export type IGlobalRtkState = {
    status: IStatusRtkState;
    auth: IAuthRtkState;
};

export const globalReducers = combineReducers<IGlobalRtkState>({
    status: StatusReducer,
    auth: AuthReducer,
});
