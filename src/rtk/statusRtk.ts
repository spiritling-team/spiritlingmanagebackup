import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface IStatusRtkState {
    loading: boolean;
    loadingText: string;
    appInit: boolean;
}

const initialState: IStatusRtkState = {
    loading: false,
    loadingText: "正在加载中...",
    appInit: false,
};

export const StatusSlice = createSlice({
    name: "Status",
    initialState,
    reducers: {
        setLoadingStatus: (state, action: PayloadAction<boolean>) => {
            state.loading = action.payload;
            return state;
        },
        setLoadingText: (state, action: PayloadAction<string>) => {
            state.loadingText = action.payload;
            return state;
        },
        setLoading: (
            state,
            action: PayloadAction<{ loadingText: string; loading: boolean }>
        ) => {
            state.loadingText = action.payload.loadingText;
            state.loading = action.payload.loading;
            return state;
        },
        setAppInit: (state, action: PayloadAction<boolean>) => {
            state.appInit = action.payload;
            return state;
        },
    },
});

// Action creators are generated for each case reducer function
export const { setLoading, setLoadingStatus, setLoadingText, setAppInit } =
    StatusSlice.actions;

export default StatusSlice.reducer;
