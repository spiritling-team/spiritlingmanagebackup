export interface IAuthToken {
    access_token: string;
    id_token: string;
    scope: string;
    token_type: string;
    expires_in: number;
}

// {
//     "id_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6IkFCRDc4NDZCQzZDREYwMjVEOEVEQjk2QjI0NDUzNDI0IiwidHlwIjoiSldUIn0.eyJuYmYiOjE2MjYzMzg3NDAsImV4cCI6MTYyNjM1MzE0MCwiaXNzIjoiaHR0cHM6Ly9sb2NhbGhvc3Q6NDQzNTgiLCJhdWQiOiJNYW5hZ2VyU2l0ZSIsImlhdCI6MTYyNjMzODc0MCwiYXRfaGFzaCI6InhkeE81UElrVEM4eC16dFY2TnozSFEiLCJzX2hhc2giOiJUTDhHV0pHTGlpd0ZLSl9FZ1hJQTlRIiwic2lkIjoiRUFBRDg1NUVENzkwMTRDQjZEMzkxRjEyNzg2NTE3MEMiLCJzdWIiOiI2YmZiZmRlNC02ZDU2LTQyNjUtYWEzNC1hYjY3NmY3YTUyNmMiLCJhdXRoX3RpbWUiOjE2MjYzMzI2OTUsImlkcCI6ImxvY2FsIiwiYW1yIjpbInB3ZCJdfQ.f-FWbqKWDnV7RmaDucklwKNOfFvljKxBBK-V8VjK8Cml9a6vE0WJ_qW-CNxCD1GWomrwEupbe6nkLdcUXBNLoN5xQio9smJXxVaC_YanxFwrkmcFT6JF8VSEPOhyoYSfE0ew5zdgHbjh9gRT-Vl8mxcqKtif29XzS3mxFGU-Q2kL_lAroBh0VfFzrX7j67xQ_0NzxU38bDMHH8nCIjFc-lsXYv6sWs5MBHfJ6Us7eS3KGnyzFdW1KvffA6EpISPbVCOzWoWv18PE8lFqB-CXoQcvL-H_U8Fmlucedr30w_4BopH6uwtsRpgdE_WWothLLZgylq5CIQ5nvEvY4eEewA",
//     "access_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6IkFCRDc4NDZCQzZDREYwMjVEOEVEQjk2QjI0NDUzNDI0IiwidHlwIjoiYXQrand0In0.eyJuYmYiOjE2MjYzMzg3NDAsImV4cCI6MTYyNjM1MzE0MCwiaXNzIjoiaHR0cHM6Ly9sb2NhbGhvc3Q6NDQzNTgiLCJjbGllbnRfaWQiOiJNYW5hZ2VyU2l0ZSIsInN1YiI6IjZiZmJmZGU0LTZkNTYtNDI2NS1hYTM0LWFiNjc2ZjdhNTI2YyIsImF1dGhfdGltZSI6MTYyNjMzMjY5NSwiaWRwIjoibG9jYWwiLCJ1c2Vya2V5IjoiMGZjMDVhNDktODJhOS00NWJkLWEzODYtMDZmNTAxODk3ZWVlIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTA3LTA5VDE3OjE2OjE2IiwidXBkYXRlZF9hdCI6IjIwMjEtMDctMDlUMTc6MTY6MTgiLCJqdGkiOiJDRjk0QTg0NkEwQzNDRUI3MUZBRjFENDVGQUQ3RkI2RSIsInNpZCI6IkVBQUQ4NTVFRDc5MDE0Q0I2RDM5MUYxMjc4NjUxNzBDIiwiaWF0IjoxNjI2MzM4NzQwLCJzY29wZSI6WyJvcGVuaWQiLCJwcm9maWxlIiwiTWFuYWdlckFwaSJdLCJhbXIiOlsicHdkIl19.G7wEmPzjb3MKqMHO802kO7fqVe7R3ABz_xx2bCmLm7jYwjEvvCNsrDHob5LfCWadu8yGd32Q1cDHxu0Y1Sv_WG3mr1qj_qpsZWE019GIYhvR47mG8wZOYirPn98OFnga3UxGld1TgqIr7XMJnDJs4IzfvlYtlNE6rEw2apEEZZf2dLiNJOHTh04vsRPtMZiY2BiRJ5UnZRfZ1M-tYMF6khh3l53UBds0EyztV7KIpVHLyFdNWmB3N-LHWp04GvxY6WgzK2ZzcDaMouOmpfLK526RptIq8GOOHGXgkkaXo4u-OHUROH19w5g3pO7I6ka4PMDATnXD4WkalsTKEcZC_g",
//     "expires_in": 14400,
//     "token_type": "Bearer",
//     "scope": "openid profile ManagerApi"
// }

export interface IUserInfo {
    sub: string;
    userkey: string;
    status: string;
    created_at: string;
    updated_at: string;
}

// {
//     "sub": "6bfbfde4-6d56-4265-aa34-ab676f7a526c",
//     "userkey": "0fc05a49-82a9-45bd-a386-06f501897eee",
//     "status": "1",
//     "created_at": "2021-07-09T08:16:16.0000000Z",
//     "updated_at": "2021-07-09T08:16:18.0000000Z"
// }

export interface IdentityServerConfiguration {
    /**
     * @description 站点地址
     */
    issuer: string;
    jwks_uri: string;
    /**
     * @description 认证地址
     */
    authorization_endpoint: string;
    /**
     * @description token获取地址
     */
    token_endpoint: string;
    /**
     * @description 用户信息地址
     */
    userinfo_endpoint: string;
    /**
     * @description 结束登录地址
     */
    end_session_endpoint: string;
    check_session_iframe: string;
    revocation_endpoint: string;
    introspection_endpoint: string;
    device_authorization_endpoint: string;
    frontchannel_logout_supported: boolean;
    frontchannel_logout_session_supported: boolean;
    backchannel_logout_supported: boolean;
    backchannel_logout_session_supported: boolean;
}

export interface IGetTokenParams {
    client_id: string;
    code: string;
    redirect_uri: string;
    code_verifier: string;
    grant_type: string;
    endPoint?: string;
}
