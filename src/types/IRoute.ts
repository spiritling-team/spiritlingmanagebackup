export interface RouteOption {
    exact?: boolean;
    sensitive?: boolean;
    strict?: boolean;
}

export interface IRoute {
    //对于后端 Menu 没有配置的页面： 比如通知，登录履历 需要在 Route 中配置 name, 供面包屑导航使用
    name?: string;
    component: () => NonNullable<JSX.Element>;
    path: string;
    routes?: IRoute[];
    routeOption?: RouteOption;
}
