import React, { useEffect } from "react";
import { Button, Result } from "antd";
import { getHostPath } from "utils/envDetect";
import Logger from "utils/_logger";
import { useDispatch, useSelector } from "react-redux";
import { statusSelector } from "store/selector";
import { getIdsConfig } from "utils/getStore";
import { getConnectAuthorize } from "utils/endpoint";
import _ from "lodash";
import { setLoadingStatus } from "rtk/statusRtk";
import { useBoolean } from "ahooks";

const SignOutCallback = () => {
    const [isError, { setTrue, setFalse }] = useBoolean(false);
    const dispatch = useDispatch();
    const status = useSelector(statusSelector);
    useEffect(
        function () {
            const init = async function () {
                setFalse();
                dispatch(setLoadingStatus(true));
                var result = await new Promise<string>((resolve, reject) => {
                    setTimeout(function () {
                        const idsConfig = getIdsConfig();
                        let url = "";
                        if (idsConfig) {
                            // 路径不为单独路由，则继续进入，内部处理
                            url = getConnectAuthorize(
                                idsConfig.authorization_endpoint
                            );
                            resolve(url);
                        } else {
                            resolve("");
                        }
                    }, 2000);
                });
                if (!_.isEmpty(result)) {
                    window.open(result, "_self");
                    setFalse();
                } else {
                    dispatch(setLoadingStatus(false));
                    setTrue();
                }
            };
            try {
                if (status.appInit) {
                    init();
                }
            } catch (err) {
                Logger.LogInformation("SignOutCallback", "Init", err);
            }
        },
        [dispatch, status.appInit]
    );
    return (
        <React.Fragment>
            {isError && (
                <Result
                    status={"error"}
                    title={"退出失败"}
                    subTitle={"请联系管理员进行解决"}
                    extra={
                        <Button
                            onClick={() => {
                                window.open(`${getHostPath()}`, "_self");
                            }}
                        >
                            Go Home
                        </Button>
                    }
                />
            )}
        </React.Fragment>
    );
};

export default SignOutCallback;
