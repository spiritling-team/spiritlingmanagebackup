import { IRouter } from "utils/routerUtils";
import { RouterPath } from "consts/RouterAboutName";
import SignOutCallback from "routes/PublicService/SignOutCallback/SignOutCallback";

const SignOutCallbackRouter: IRouter = {
    path: RouterPath.SignOutCallback,
    name: RouterPath.SignOutCallback,
    component: SignOutCallback,
    routeOption: {
        exact: true,
    },
};

export default SignOutCallbackRouter;
