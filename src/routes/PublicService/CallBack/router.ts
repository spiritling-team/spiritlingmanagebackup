import { IRouter } from "utils/routerUtils";
import CallBack from "routes/PublicService/CallBack/CallBack";
import { RouterName, RouterPath } from "consts/RouterAboutName";

const CallBackRouter: IRouter = {
    path: RouterPath.Callback,
    name: RouterName.Callback,
    component: CallBack,
    routeOption: {
        exact: true,
    },
};

export default CallBackRouter;
