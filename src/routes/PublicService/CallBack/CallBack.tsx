import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { setLoadingStatus } from "rtk/statusRtk";
import { authSelector, routerSelector, statusSelector } from "store/selector";
import _ from "lodash";
import { ManageSiteClientID, ManageSiteRedirectUri } from "consts/GlobalName";
import { getTokenAsync } from "rtk/authRtk";
import { localStorageUtils } from "utils/localStorageUtils";
import {
    LocalAuthAccessToken,
    LocalAuthCodeVerifier,
    LocalAuthToken,
} from "consts/localStorageName";
import { useAppDispatch } from "utils/appDispatchAndSelector";
import { IAuthToken } from "types/authInterface";
import { getHostPath } from "utils/envDetect";
import { Button, Result } from "antd";
import Logger from "utils/_logger";
import { useBoolean } from "ahooks";

const CallBack = () => {
    const [isError, { setTrue, setFalse }] = useBoolean(false);
    const dispatch = useAppDispatch();
    const router = useSelector(routerSelector);
    const authState = useSelector(authSelector);
    const status = useSelector(statusSelector);
    useEffect(
        function () {
            const init = async function () {
                const search = _.cloneDeep(router.location.query) as {
                    code: string;
                    scope: string;
                    state: string;
                    session_state: string;
                };
                search.scope = decodeURIComponent(search.scope);
                if (search?.code) {
                    const code_verifier =
                        localStorageUtils.getItem(LocalAuthCodeVerifier) ?? "";
                    const result = await dispatch(
                        getTokenAsync({
                            client_id: ManageSiteClientID,
                            code: search.code,
                            redirect_uri: ManageSiteRedirectUri,
                            code_verifier,
                            grant_type: "authorization_code",
                            endPoint: authState.idsConfig?.token_endpoint ?? "",
                        })
                    );

                    if (result.payload) {
                        var _authToken = result.payload as IAuthToken;
                        localStorageUtils.setItem(
                            LocalAuthAccessToken,
                            _authToken.access_token
                        );
                        localStorageUtils.setItemByAny(
                            LocalAuthToken,
                            _authToken
                        );
                        // 需要游览器级别的跳转，否则不会出发App中的数据初始化加载
                        window.open(`${getHostPath()}`, "_self");
                    } else {
                        dispatch(setLoadingStatus(false));
                        setTrue();
                    }
                } else {
                    dispatch(setLoadingStatus(false));
                    setTrue();
                }
            };
            try {
                if (status.appInit) {
                    init();
                }
            } catch (err) {
                Logger.LogInformation("CallBack", "Init", err);
            }
        },
        [
            authState.idsConfig?.token_endpoint,
            dispatch,
            router.location.query,
            status.appInit,
        ]
    );
    return (
        <React.Fragment>
            {isError && (
                <Result
                    status={"error"}
                    title={"授权失败"}
                    subTitle={"请联系管理员进行授权"}
                    extra={
                        <Button
                            onClick={() => {
                                window.open(`${getHostPath()}`, "_self");
                            }}
                        >
                            Go Home
                        </Button>
                    }
                />
            )}
        </React.Fragment>
    );
};

export default CallBack;
