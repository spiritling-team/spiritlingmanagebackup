import { IRouter } from "utils/routerUtils";
import CallBackRouter from "routes/PublicService/CallBack/router";
import SignOutRouter from "routes/PublicService/SignOut/router";
import SignOutCallbackRouter from "routes/PublicService/SignOutCallback/router";

const PublicRoutes: IRouter[] = [
    CallBackRouter,
    SignOutRouter,
    SignOutCallbackRouter,
];

export default PublicRoutes;
