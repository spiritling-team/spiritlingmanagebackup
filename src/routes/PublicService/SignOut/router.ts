import { IRouter } from "utils/routerUtils";
import { RouterName, RouterPath } from "consts/RouterAboutName";
import SignOut from "routes/PublicService/SignOut/SignOut";

const SignOutRouter: IRouter = {
    path: RouterPath.SignOut,
    name: RouterName.SignOut,
    component: SignOut,
    routeOption: {
        exact: true,
    },
};

export default SignOutRouter;
