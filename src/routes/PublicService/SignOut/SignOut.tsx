import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { authSelector, statusSelector } from "store/selector";
import { setLoadingStatus } from "rtk/statusRtk";
import { localStorageUtils } from "utils/localStorageUtils";
import { LocalAuthAccessToken } from "consts/localStorageName";
import { getEndSession } from "utils/endpoint";
import { getIdToken } from "utils/getStore";
import Logger from "utils/_logger";

const SignOut = () => {
    const status = useSelector(statusSelector);
    const dispatch = useDispatch();
    const authState = useSelector(authSelector);
    useEffect(
        function () {
            const init = async function () {
                dispatch(setLoadingStatus(true));
                localStorageUtils.removeItem(LocalAuthAccessToken);
                const url = getEndSession({
                    accessToken: getIdToken(),
                    baseUrl: `${authState.idsConfig?.end_session_endpoint}`,
                });
                window.open(url, "_self");
                dispatch(setLoadingStatus(false));
            };
            try {
                if (status.appInit) {
                    init();
                }
            } catch (err) {
                Logger.LogInformation("SignOut", "Init", err);
            }
        },
        [authState.idsConfig?.end_session_endpoint, dispatch, status.appInit]
    );
    return <React.Fragment></React.Fragment>;
};

export default SignOut;
