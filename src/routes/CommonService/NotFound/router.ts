import { IRouter } from "utils/routerUtils";
import NotFound from "routes/CommonService/NotFound/NotFound";
import { RouterName, RouterPath } from "consts/RouterAboutName";

const NotFoundRouter: IRouter = {
    path: RouterPath.NotFound,
    name: RouterName.NotFound,
    component: NotFound,
    routeOption: {
        exact: false,
    },
};

export default NotFoundRouter;
