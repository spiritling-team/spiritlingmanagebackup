import { IRouter } from "utils/routerUtils";
import DashboardRouter from "routes/CommonService/Dashboard/router";

const CommonRoutes: IRouter[] = [DashboardRouter];

export default CommonRoutes;
