import { IRouter } from "utils/routerUtils";
import { RouterName, RouterPath } from "consts/RouterAboutName";
import { lazy } from "react";

const DashboardLayz = lazy(
    () => import("routes/CommonService/Dashboard/Dashboard")
);

const DashboardRouter: IRouter = {
    path: RouterPath.Dashboard,
    name: RouterName.Dashboard,
    component: DashboardLayz,
    routeOption: {
        exact: true,
    },
};

export default DashboardRouter;
