import { IRouter } from "utils/routerUtils";
import { RouterName, RouterPath } from "consts/RouterAboutName";
import { lazy } from "react";

const ClientDetailLazy = lazy(
    () => import("routes/ManageService/Client/ClientDetail/ClientDetail")
);

const ClientDetailRouter: IRouter = {
    path: RouterPath.ClientDetail,
    name: RouterName.ClientDetail,
    component: ClientDetailLazy,
    routeOption: {
        exact: false,
    },
};

export default ClientDetailRouter;
