import React from "react";
import { Space } from "antd";
import { Link } from "react-router-dom";
import { RouterDynamic } from "consts/RouterAboutName";

const Client = () => {
    return (
        <section>
            <Space>
                <Link to={RouterDynamic.ClientDetail("1")}>111111</Link>
                <Link to={RouterDynamic.ClientDetail("2")}>222222</Link>
                <Link to={RouterDynamic.ClientDetail("3")}>333333</Link>
            </Space>
        </section>
    );
};

export default Client;
