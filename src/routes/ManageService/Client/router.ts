import { IRouter } from "utils/routerUtils";
import { RouterName, RouterPath } from "consts/RouterAboutName";
import ClientDetailRouter from "routes/ManageService/Client/ClientDetail/router";
import { lazy } from "react";

const ClientLazy = lazy(() => import("routes/ManageService/Client/Client"));

const ClientRouter: IRouter[] = [
    {
        path: RouterPath.Client,
        name: RouterName.Client,
        component: ClientLazy,
        routeOption: {
            exact: true,
        },
        routes: [ClientDetailRouter],
    },
];

export default ClientRouter;
