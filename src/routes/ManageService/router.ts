import { IRouter } from "utils/routerUtils";
import ClientRouter from "routes/ManageService/Client/router";

const ManageRoutes: IRouter[] = [...ClientRouter];

export default ManageRoutes;
