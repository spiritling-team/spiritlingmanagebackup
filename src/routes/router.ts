import PublicRoutes from "routes/PublicService/router";
import NotFoundRouter from "routes/CommonService/NotFound/router";
import { IRouterConfig } from "utils/routerUtils";
import ManageRoutes from "routes/ManageService/router";
import CommonRoutes from "routes/CommonService/router";

const routerConfig: IRouterConfig = {
    publicRouter: PublicRoutes,
    manageRouter: [...CommonRoutes, ...ManageRoutes],
};

// eslint-disable-next-line
console.log(routerConfig);

routerConfig.manageRouter.push(NotFoundRouter);
export default routerConfig;
