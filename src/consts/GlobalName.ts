import { getHostPath } from "utils/envDetect";

export const ManageSiteClientID = "ManageSite";

export const ManageSiteRedirectUri = `${getHostPath()}/callback`;

export const ManageSiteLogoutRedirectUri = `${getHostPath()}/signoutcallback`;

export const ManageSiteScope = "openid profile ManagerApi";
