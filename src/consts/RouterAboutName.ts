export const RouterPath = {
    Dashboard: "/dashboard",
    Client: "/client",
    ClientDetail: "/clientDetail/:id",
    Callback: "/callback",
    SignOut: "/signout",
    SignOutCallback: "/signoutcallback",
    NotFound: "*",
};

export const RouterName = {
    Dashboard: "仪表板",
    Client: "客户端",
    ClientDetail: "客户端详细",
    Callback: "登录回调",
    SignOut: "退出页面",
    SignOutCallback: "退出回调",
    NotFound: "Not Found",
};

export const RouterDynamic = {
    ClientDetail: (id: string) => {
        return RouterPath.ClientDetail.replace(":id", id);
    },
};
