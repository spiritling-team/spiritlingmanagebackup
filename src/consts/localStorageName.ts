export const LocalStoragePrefix = "sl_ms_";
export const LocalAuthState = "authState";
export const LocalAuthCodeVerifier = "authCodeVerifier";
export const LocalAuthAccessToken = "authAccessToken";
export const LocalAuthToken = "authToken";
export const LocalAuthConfig = "authConfig";
export const LocalAuthUserInfo = "authUserInfo";
