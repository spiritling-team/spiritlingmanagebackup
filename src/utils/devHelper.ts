import {Store} from "@reduxjs/toolkit";

export const initDevHelper = (store:Store)=>{
    window._devHelper = {
        get state() {
            return store.getState();
        },
    }
}
