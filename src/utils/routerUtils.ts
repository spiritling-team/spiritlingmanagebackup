import { LazyExoticComponent } from "react";
import PublicRoutes from "routes/PublicService/router";

export interface IRouterOption {
    exact?: boolean;
    sensitive?: boolean;
    strict?: boolean;
}

export interface IRouterConfig {
    publicRouter: IRouter[];
    manageRouter: IRouter[];
}

export interface IRouter {
    path: string;
    name: string;
    component:
        | (() => NonNullable<JSX.Element>)
        | LazyExoticComponent<() => NonNullable<JSX.Element>>;
    routes?: IRouter[];
    routeOption?: IRouterOption;
}
