import { LocalStoragePrefix } from "consts/localStorageName";

export const localStorageUtils = {
    /**
     * @description 获取字符串值
     * @param name
     */
    getItem: (name: string): string | null => {
        return localStorage.getItem(`${LocalStoragePrefix}${name}`);
    },
    getItemForArray: <T>(name: string): T[] | undefined => {
        const value = localStorage.getItem(`${LocalStoragePrefix}${name}`);
        return value ? (JSON.parse(value) as T[]) : undefined;
    },
    getItemForAny: <T>(name: string): T | undefined => {
        const value = localStorage.getItem(`${LocalStoragePrefix}${name}`);
        return value ? (JSON.parse(value) as T) : undefined;
    },
    setItem: (name: string, value: string) => {
        localStorage.setItem(`${LocalStoragePrefix}${name}`, value);
    },
    setItemByArray: <T>(name: string, value: T[]) => {
        localStorage.setItem(
            `${LocalStoragePrefix}${name}`,
            JSON.stringify(value)
        );
    },
    setItemByAny: <T>(name: string, value: T) => {
        localStorage.setItem(
            `${LocalStoragePrefix}${name}`,
            JSON.stringify(value)
        );
    },
    removeItem: (name: string) => {
        localStorage.removeItem(`${LocalStoragePrefix}${name}`);
    },
};
