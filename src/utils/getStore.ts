import { store } from "store/store";
import { IdentityServerConfiguration } from "types/authInterface";

export const getAccessToken = (): string =>
    store.getState().global.auth?.token?.access_token ?? "";

export const getIdToken = (): string =>
    store.getState().global.auth?.token?.id_token ?? "";

export const getIdsConfig = (): IdentityServerConfiguration | undefined =>
    store.getState().global.auth.idsConfig;
