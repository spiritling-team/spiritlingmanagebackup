import { v4 as uuid } from "uuid";
import { localStorageUtils } from "utils/localStorageUtils";
import CryptoJS from "crypto-js";
import {
    LocalAuthAccessToken,
    LocalAuthCodeVerifier,
    LocalAuthState,
} from "consts/localStorageName";
import {
    ManageSiteClientID,
    ManageSiteLogoutRedirectUri,
    ManageSiteRedirectUri,
    ManageSiteScope,
} from "consts/GlobalName";

export const getConnectAuthorize = (connectAuthorizeUrl: string) => {
    const state = uuid();
    const code = uuid() + "-" + uuid();
    localStorageUtils.setItem(LocalAuthState, state);
    localStorageUtils.setItem(LocalAuthCodeVerifier, code);
    return `${connectAuthorizeUrl}?client_id=${ManageSiteClientID}&redirect_uri=${encodeURIComponent(
        ManageSiteRedirectUri
    )}&response_type=code&scope=${ManageSiteScope}&state=${uuid()}&code_challenge=${generateCodeChallenge(
        code
    )}&code_challenge_method=S256&response_mode=query`;
};

export const generateCodeChallenge = (code_verifier: string) => {
    return base64Url(CryptoJS.SHA256(code_verifier));
};

export const base64Url = (str: CryptoJS.lib.WordArray): string => {
    return str
        .toString(CryptoJS.enc.Base64)
        .replace(/=/g, "")
        .replace(/\+/g, "-")
        .replace(/\//g, "_");
};

export const getEndSession = (endSession: {
    accessToken: string;
    baseUrl: string;
}) => {
    localStorageUtils.removeItem(LocalAuthAccessToken);
    return `${endSession.baseUrl}?id_token_hint=${
        endSession.accessToken
    }&post_logout_redirect_uri=${encodeURIComponent(
        ManageSiteLogoutRedirectUri
    )}`;
};
