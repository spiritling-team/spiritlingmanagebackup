import * as Sentry from "@sentry/react";

const basic = (
    reducerName: string,
    asyncThunkName: string,
    error: any,
    level: Sentry.Severity
) => {
    Sentry.setTag("Reducer", reducerName);
    Sentry.setTag("AsyncThunk", asyncThunkName);
    Sentry.captureMessage(error, level);
};

/**
 * @description 日志等级：*
 */
const LogInformation = (
    reducerName: string,
    asyncThunkName: string,
    error: any
) => {
    basic(reducerName, asyncThunkName, error, Sentry.Severity.Info);
};

/**
 * @description 日志等级：**
 */
const LogTrace = (reducerName: string, asyncThunkName: string, error: any) => {
    basic(reducerName, asyncThunkName, error, Sentry.Severity.Log);
};

/**
 * @description 日志等级：***
 */
const LogWarning = (
    reducerName: string,
    asyncThunkName: string,
    error: any
) => {
    basic(reducerName, asyncThunkName, error, Sentry.Severity.Warning);
};

/**
 * @description 日志等级：****
 */
const LogError = (reducerName: string, asyncThunkName: string, error: any) => {
    basic(reducerName, asyncThunkName, error, Sentry.Severity.Error);
};

/**
 * @description 日志等级：*****
 */
const LogFatal = (reducerName: string, asyncThunkName: string, error: any) => {
    basic(reducerName, asyncThunkName, error, Sentry.Severity.Fatal);
};

const Logger = {
    LogInformation,
    LogTrace,
    LogWarning,
    LogError,
    LogFatal,
};

export default Logger;
