import React from "react";
import styles from "layout/components/index.module.scss";
import { Layout } from "antd";
import LeftMenu from "layout/components/LeftMenu";
import TopBreadcrumb from "layout/components/TopBreadcrumb";
import LayoutHeader from "layout/components/LayoutHeader";

const { Header, Sider, Content } = Layout;

interface ILayoutProps {
    children?: JSX.Element;
}

const LayoutIndex = (props: ILayoutProps) => {
    return (
        <Layout className={styles.layout}>
            <LayoutHeader />
            <Layout className={styles.layoutMain}>
                <Sider
                    theme={"light"}
                    collapsible
                    className={styles.sider}
                    width={250}
                >
                    <LeftMenu />
                </Sider>
                <Layout className={styles.contentWrapper}>
                    <TopBreadcrumb />
                    <Content className={styles.content}>
                        {/* 每个页面开始必须以section包含，自定义section控制是否滚动 */}
                        {props.children}
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    );
};

export default LayoutIndex;
