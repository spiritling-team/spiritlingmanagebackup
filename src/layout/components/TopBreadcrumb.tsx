import React from "react";
import { Breadcrumb, Skeleton } from "antd";
import styles from "layout/components/index.module.scss";
import { Link } from "react-router-dom";

const TopBreadcrumb = () => {
    const loading = false;
    return (
        <React.Fragment>
            {loading ? (
                <Skeleton.Input
                    active
                    size={"small"}
                    className={styles.breadcrumbSkeleton}
                />
            ) : (
                <Breadcrumb className={styles.breadcrumb}>
                    <Breadcrumb.Item>
                        <Link to={"/dashboard"}>Dashboard</Link>
                    </Breadcrumb.Item>
                </Breadcrumb>
            )}
        </React.Fragment>
    );
};

export default TopBreadcrumb;
