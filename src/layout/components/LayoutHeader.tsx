import React from "react";
import { Layout, Modal, Tooltip } from "antd";
import styles from "layout/components/index.module.scss";
import { LogoutOutlined, QuestionCircleOutlined } from "@ant-design/icons";
import { LocalAuthAccessToken } from "consts/localStorageName";
import { setLoadingStatus } from "rtk/statusRtk";
import { localStorageUtils } from "utils/localStorageUtils";
import { useDispatch, useSelector } from "react-redux";
import { getEndSession } from "utils/endpoint";
import { ManageSiteLogoutRedirectUri } from "consts/GlobalName";
import { authSelector } from "store/selector";
import { getIdToken } from "utils/getStore";
import { push } from "connected-react-router";
import { RouterPath } from "consts/RouterAboutName";
const { Header } = Layout;
const { confirm } = Modal;
const LayoutHeader = () => {
    const dispatch = useDispatch();
    const authState = useSelector(authSelector);
    const logoutConfirm = function () {
        confirm({
            title: "是否退出登录?",
            icon: <QuestionCircleOutlined color={"#fa8c16"} />,
            onOk() {
                dispatch(push(RouterPath.SignOut));
            },
        });
    };
    return (
        <React.Fragment>
            <Header className={styles.header}>
                <Tooltip title={"退出登录"}>
                    <LogoutOutlined onClick={logoutConfirm} />
                </Tooltip>
            </Header>
        </React.Fragment>
    );
};

export default LayoutHeader;
