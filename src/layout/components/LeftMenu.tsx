import React from "react";
import styles from "layout/components/index.module.scss";
import { Menu, Skeleton } from "antd";
import { Link } from "react-router-dom";
import { RouterName, RouterPath } from "consts/RouterAboutName";

const LeftMenu = () => {
    return (
        <Skeleton loading={false} active>
            <Menu mode='inline' className={styles.siderMenu}>
                <Menu.Item key={RouterPath.Dashboard}>
                    <Link to={RouterPath.Dashboard}>
                        {RouterName.Dashboard}
                    </Link>
                </Menu.Item>
                <Menu.Item key={RouterPath.Client}>
                    <Link to={RouterPath.Client}>{RouterName.Client}</Link>
                </Menu.Item>
            </Menu>
        </Skeleton>
    );
};

export default LeftMenu;
