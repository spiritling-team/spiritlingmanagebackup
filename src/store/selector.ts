import { Selector } from "react-redux";
import { RouterState } from "connected-react-router";
import { RootState } from "store/rtk";
import { IAuthRtkState } from "rtk/authRtk";
import { IStatusRtkState } from "rtk/statusRtk";

export const routerSelector: Selector<RootState, RouterState> = (state) =>
    state.router;

export const authSelector: Selector<RootState, IAuthRtkState> = (state) =>
    state.global.auth;

export const statusSelector: Selector<RootState, IStatusRtkState> = (state) =>
    state.global.status;
