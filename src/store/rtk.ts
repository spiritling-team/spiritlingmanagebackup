import { History } from "history";
import {
    AnyAction,
    CombinedState,
    combineReducers,
    Reducer,
} from "@reduxjs/toolkit";
import { connectRouter, RouterState } from "connected-react-router";
import { IGlobalRtkState, globalReducers } from "rtk";

export type RootState = {
    router: RouterState;
    global: IGlobalRtkState;
};

const createRootReducer = (
    history: History
): Reducer<CombinedState<RootState>, AnyAction> =>
    combineReducers<RootState>({
        router: connectRouter(history),
        global: globalReducers,
    });

export default createRootReducer;
