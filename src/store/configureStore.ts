import * as historyPkg from "history";
import { routerMiddleware } from "connected-react-router";
import { configureStore } from "@reduxjs/toolkit";
import logger from "redux-logger";
import createRootReducer from "./rtk";
import { getRelativeBasePath, isDev } from "utils/envDetect";

export const history = historyPkg.createBrowserHistory({
    basename: getRelativeBasePath(),
    keyLength: 32,
});

const reducer = createRootReducer(history);

export const getStore = () => {
    return configureStore({
        reducer,
        middleware: (getDefaultMiddleware) => {
            const defaultMiddleware = getDefaultMiddleware();
            let middlewares = defaultMiddleware.concat(
                routerMiddleware(history)
            );
            if (isDev) {
                middlewares = middlewares.concat(logger);
            }
            return middlewares;
        },
        devTools: isDev,
        preloadedState: {},
    });
};
