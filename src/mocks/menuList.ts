export interface MenuItem {
    id: number;
    name: string;
    path?: string;
    icon?: string;
    parentId?: number | undefined;
    children?: MenuItem[] | undefined;
}

export const menuList: MenuItem[] = [
    {
        id: 11000,
        name: "仪表板",
        path: "/dashboard",
        icon: "dashboard",
    },
    {
        id: 11010,
        name: "Test1",
    },
    {
        id: 11011,
        name: "Test1",
        path: "/test1",
        parentId: 11010,
    },
    {
        id: 11012,
        name: "Test2",
        path: "/test2",
        parentId: 11010,
    },
];
